# Realizzare un sito... in 10 minuti

L'obiettivo di questo progetto è produrre un tutorial leggibile 
in una decina di minuti in cui si descrive l'uso di: 

* un repository (git + bitbucket) per contenere il progetto 
  di un sito;
* Sphinx per tradurre un insieme di testi in un sito composto 
  da pagine html;
* come pubblicarlo su Bitbucket.org.
 
Per produrre un sito con immagini, tabelle e formule matematiche
probabilmente serviranno più di 10 minuti!

## Link

Il lavoro è ospitato in:

[bitbucket.org/danzam10min/sito10min](https://bitbucket.org/danzam10min/sito10min)

Il risultato del lavoro lo si può vedere in:

[danzam10min/bitbucket.io/](https://danzam10min/bitbucket.io/)

## Licenza

Tutto il materiale di questo progetto è rilasciato sotto 
licenza: CC-BY-SA.

## Contatti

daniele.zambelli@gmail.com
