Contenuti
=========

Ora che abbiamo installato gli strumenti e organizzato il lavoro pensiamo 
a riempire di contenuti le nostre pagine.

Index
-----

L'unico file creato da ``sphinx-quickstart`` è ``index.rst`` per ora 
contiene un po’ poco...

Questo file contiene l'elenco delle sezioni principali del documento e 
ne produce l'indice principale.
Man mano che vengono delle nuove idee si possono aggiungere righe. 
Si possono anche scrivere dei riferimenti a file vuoti che verranno 
riempiti man mano. 

Riporto di seguito il contenuto del file ``index.rst``:

.. literalinclude:: index.rst

Ovviamente nella stessa directory di ``index.rst`` chi sono anche i 
file in cui ho scritto le prime sezioni del documento e altri file 
attualmente vuoti.
La directory ``source`` ha il seguente contenuto::

  .
  ├── 00intro.rst
  ├── 01strumenti.rst
  ├── 02organizzazione.rst
  ├── 03contenuti.rst
  ├── 04link.rst
  ├── 05pubblicazione.rst
  ├── conf.py
  ├── index.rst
  ├── _static
  └── _templates

Ora posso compilare i vari file, in realtà scrivendo questo testo ho già riempito i primi tre e sto riempiendo ``03contenuti.rst``

Compilare e controllare
-----------------------

Una volta incominciato a inserire i primi contenuti, le prime righe 
compilo e poi controllo di aver ottenuto il risultato desiderato.

In un terminale aperto nella directory dove si trova ``Makefile`` 
provo a compilare con il comando::

  $> make html

all'inizio si otterrà una serie di errori, un po’ alla volta interpretando i messaggi scritti in rosso dal compilatore e modificando il sorgente sono arrivato a una compilazione *pulita*.

A questo punto apro in un browser il file ``build/html/index.html`` e 
controllo il risultato del mio lavoro.
Decido quindi se aggiungere, togliere o modificare.

Struttura
---------

È possibile strutturare il testo in 
sezione, sottosezione, sottosottosezione, ...

I diversi livelli della struttura sono definiti dal tipo di sottolineatura
dei titoli::

   Titolo della Sezione
   ====================

   Titolo della Sottosezione
   -------------------------

   Titolo della Sottosezione
   .........................

   Testo normale

Immagini
--------

Possiamo inserire immagini separate dal testo con una didascalia:

.. figure:: _static/03img/libri01.jpg
   :scale: 90 %

   Libri

Immagine ottenuta con::

  .. figure:: _static/03img/libri01.jpg
    :scale: 90 %

    Libri

.. figure:: _static/03img/libri02.jpg
   :height: 150px
   :width: 150px
   :scale: 80 %
   :alt:  altri libri
   :align: right
   :figwidth: 300px

   Altri Libri

Le immagini possono anche essere inserite all'interno di un paragrafo
aggiungendo una serie di parametri opzionali che permettono di
specificare diverse caratteristiche della figura come le dimensioni, un 
testo alternativo, l'allineamento...

Immagine ottenuta con::

  .. figure:: _static/03img/libri02.jpg
    :height: 150px
    :width: 150px
    :scale: 80 %
    :alt:  altri libri
    :align: right
    :figwidth: 300px

    Altri Libri

Tabelle
-------

Facilmente in un testo scientifico devono essere inserite delle tabelle. Sphinx permette di descriverle o disegnandole con i caratteri 
``+  -  |``:

+-------------------+-----------+-----------+------------+
| riga 1, colonna 1 | colonna 2 | colonna 3 | colonna 4  |
+-------------------+-----------+-----------+------------+
| riga 2            | Fusione di due celle  | altro      |
+-------------------+-----------+-----------+------------+
| riga 3            |           |           |            |
+-------------------+-----------+-----------+------------+

La faccenda più rognosa: bisogna stare attenti al numero di caratteri usato 
per definire le tabelle che deve corrispondere nelle varie righe.

La tabella precedente è ottenuta con::

  +-------------------+-----------+-----------+------------+
  | riga 1, colonna 1 | colonna 2 | colonna 3 | colonna 4  |
  +-------------------+-----------+-----------+------------+
  | riga 2            | Fusione di due celle  | altro      |
  +-------------------+-----------+-----------+------------+
  | riga 3            |           |           |            |
  +-------------------+-----------+-----------+------------+

.. _seconda tabella:

La seguente tabella:

=====  =====  =====
  A      B    A e B
=====  =====  =====
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =====

è ottenuta in modo più semplice::

  =====  =====  =====
    A      B    A e B
  =====  =====  =====
  False  False  False
  True   False  False
  False  True   False
  True   True   True
  =====  =====  =====

Formule
-------

Se siamo, per caso interessati a scrivere qualcosa di legato alla matematica
avremo bisogno di scrivere delle definizioni e delle formule. Ad esempio se
siamo arrivati al capitolo delle potenze potremo scrivere:

.. _definizione potenza:

**Definizione** 
Dati due numeri naturali :math:`b` e :math:`e`,
il primo detto base, il secondo esponente, 
la potenza di :math:`b` con esponente :math:`e` è il numero :math:`p` 
che si ottiene moltiplicando :math:`1` per :math:`e` fattori tutti 
uguali a :math:`b`. 
Si scrive  e si legge ":math:`b`  elevato alla :math:`e` è uguale a :math:`p`":

.. math::
  b^e = 1 \underbrace{\times b \times b \times \dots \times b}_{e~~volte}

Esempio numerico: :math:`5^3 = 1 \times 5 \times 5 \times 5 = 125`

ReStructuredText ci mette a disposizione tutto l'armamentario di LaTex per
produrre tutte le formule possibili e immaginabili.

Le precedenti formule sono state ottenute dal seguente sorgente::

  .. math::
    b^e = 1 \underbrace{\times b \times b \times \dots \times b}_{e~~volte}

  Esempio numerico: :math:`5^3 = 1 \times 5 \times 5 \times 5 = 125`

Le formule sono rese in ``mathjax`` e questo permette, cliccando con il 
destro di visualizzarle o di copiarle in mathml o in latex.
 
