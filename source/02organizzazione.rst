Organizzazione
==============

Ora che abbiamo installato gli strumenti necessari e dato loro 
un'occhiata, passiamo ad organizzarci il lavoro con le operazioni che 
tipicamente verranno eseguite una sola volta in ogni progetto.

Creazione della directory di lavoro
-----------------------------------

Nella mia macchina Linux ho creato una directory dove raccogliere tutti i 
miei lavori con ``Sphinx``. Per farlo ho usato il comando::

  $> mkdir sphinx

Mi sono spostato in questa dir::

  $> cd sphinx

Creazione di un repository
--------------------------

Sono entrato nella mia pagina di ``Bitbucket`` 
(``bitbucket.org/danzam10min/``) e ho 
creato un nuovo repository cliccando sul bottone ``Create repository``.

Nella finestra di dialogo ho fatto le seguenti scelte:

Project:
  ``sito``
  
Repository name:
  ``sito10min``
  
Access level:
  ho tolto la spunta in modo da creare un repo pubblico

Advanced setting - Description:
  ho scritto una breve descrizione del progetto.
  
IMPORTANTE: anche nel nome del repository, 
ho evitato caratteri strani, spazi, punti o trattini bassi.

Una volta confermato, l'indirizzo a cui si trova questo lavoro è:

`bitbucket.org/danzam10min/sito10min 
<https://bitbucket.org/danzam10min/sito10min/>`_

Essendo pubblico, chiunque può accedervi, navigare nel sorgente, 
scaricare un file o l'intero repository in un formato compresso o 
clonare il progetto e contribuirvi.

Clonare in locale il repository
-------------------------------

Dalla pagina principale del repository ho copiato la stringa per clonare 
il repository:

Pulsante ``clone`` e poi ``copia``.

Nel terminale ho incollato (tasto destro del mouse) il comando
e l'ho eseguito::

  git clone https://danzam10min@bitbucket.org/danzam10min/sito10min.git

poi mi sono spostato nella nuova cartella ottenuta::

  $> cd sito10min

Questa dir contiene una cartella nascosta: ``.git``
e un file nascosto: ``.gitignore``.

Questo è un testo che indica a ``git`` quali file **non** tracciare.
Infatti  non voglio intasare il repository con file temporanei o creati
automaticamente, ma voglio che vengano salvati e tracciati solo i file
essenziali. 

Ho modificato ``.gitignore``, ora il suo contenuto è:

.. literalinclude:: ../.gitignore

Ma quando vedo che ``git`` si ha individuato qualche altro file, ma non 
voglio che questo venga salvato, lo aggiungo in ``.gitignore``.

Nei miei progetti creo spesso una directory, che chiamo ``_ignore``, dove 
metto tutti i materiali che possono servirmi ma che non devono essere 
tracciati. 
Ovviamente ho inserito anche questa directory nel file ``.gitignore``.

Creazione di un progetto Sphinx
-------------------------------

Ora che abbiamo un repository su ``bitbucket`` e la sua immagine locale, 
creiamo un progetto ``sphinx`` vuoto::

  $> sphinx-quickstart

Riporto le domande poste da ``sphinx-quickstart`` e le mie risposte::

  Welcome to the Sphinx 4.3.1 quickstart utility.

  Please enter values for the following settings (just press Enter to
  accept a default value, if one is given in brackets).

  Selected root path: .

  You have two options for placing the build directory for Sphinx output.
  Either, you use a directory "_build" within the root path, or you separate
  "source" and "build" directories within the root path.
  > Separate source and build directories (y/n) [n]: y

  The project name will occur in several places in the built documentation.
  > Project name: sito10min
  > Author name(s): danzam10min
  > Project release []: 0.0

  If the documents are to be written in a language other than English,
  you can select a language here by its language code. Sphinx will then
  translate text that it generates into that language.

  For a list of supported codes, see
  https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.
  > Project language [en]: it

  Creating file /.../sphinx/sito10min/source/conf.py.
  Creating file /.../sphinx/sito10min/source/index.rst.
  Creating file /.../sphinx/sito10min/Makefile.
  Creating file /.../sphinx/sito10min/make.bat.

  Finished: An initial directory structure has been created.

  You should now populate your master file 
  /.../sphinx/sito10min/source/index.rst 
  and create other documentation   source files. 
  Use the Makefile to build the docs, like so:
    make builder
  where "builder" is one of the supported builders, e.g. html, 
  latex or linkcheck.

Ora la directory ``sito10min`` presenta la seguente struttura::

  $> tree
  .
  ├── build
  ├── _ignore
  ├── make.bat
  ├── Makefile
  ├── README.md
  └── source
      ├── conf.py
      ├── index.rst
      ├── _static
      └── _templates

Dove ``build`` e ``source`` sono due directory,
``make.bat`` e ``Makefile`` sono i due file che contengono i comandi per 
compilare il contenuto della dir ``source`` e porre il risultato nella dir 
``build``.

Siamo quasi pronti per iniziare a realizzare il nostro sito, prima però 
scriviamo qualche riga per aiutare chi casualmente si imbattesse in questo 
repository per capire quali sono le nostre intenzioni e cosa può fare dopo 
averlo clonato.

Creazione del file Readme
-------------------------

Modifico il file ``README.md`` con il seguente contenuto:

.. literalinclude:: ../README.md

Il primo commit
---------------  
  
A questo punto chiediamo a ``git`` di informarci sui cambiamenti nel nostro 
repository locale::

  $> git status
  Sul branch master
  Il tuo branch è aggiornato rispetto a 'origin/master'.

  Modifiche non nell'area di staging per il commit:
    (usa "git add <file>..." per aggiornare gli elementi di cui sarà
    eseguito il commit)
    (usa "git restore <file>..." per scartare le modifiche nella directory
    di lavoro)
          modificato:             .gitignore
          modificato:             README.md

  File non tracciati:
    (usa "git add <file>..." per includere l'elemento fra quelli di cui
    verrà eseguito il commit)
          Makefile
          make.bat
          source/

  nessuna modifica aggiunta al commit (usa "git add" e/o "git commit -a")

Git ci segnala che sono presenti file non tracciati e ci suggerisce di 
usare il comando `git add` per tracciarli.

Gli diciamo di aggiungere tutto nell'area di *staging*::

  $> git add --all

Ora possiamo creare una *immaqine* a cui assegniamo un commento::

  $> git commit -m "Primo commit più README."

Ora inviamo al repo remoto su `bitbucket`, il nostro commit::

  $> git push

ma nel mio sistema ottengo::

  Password for 'https://danzam_sito10min@bitbucket.org': 
  remote: You'll need to authenticate with a valid app password. 
  You can create an app password from your account at 
  https://bitbucket.org/account/admin/app-passwords
  fatal: impossibile accedere a 
  'https://bitbucket.org/danzam_sito10min/sito10min.git/': 
  The requested URL returned error: 403

Accidenti ci scontriamo con un errore. Per uscirne dobbiamo creare 
una ``app password``... Ma come?

Cercando in ``bitbucket`` otteniamo un aiuto 
(`<support.atlassian.com/bitbucket-cloud/docs/app-passwords>`_/) 
che traduco in modo un po' fantasioso::

  1. Dal proprio avatar in basso a sinistra, clic su Personal settings.
  2. Clic su Access management - App passwords.
  3. Clic su Create app password.
  4. Dà alla app password un nome collegato all'applicazione 
     per cui la si vuole creare.
  5. Seleziona i permessi che questa password deve concedere.
  6. Copia la password generata e memorizzala o incollala nell'applicazione
     che la deve usare. La password è memorizzata qui una sola volta.

Per prima cosa ho salvato in un file di testo nella dir ``_ignore`` la 
password, in modo da averla sempre a disposizione quando serve. 

Poi ho ritentato il ``push`` e, questa volta, alla richiesta, 
ho incollato la password.

Ora su ``bitbucket`` è stato memorizzato il nostro lavoro. 

E possiamo passare a aggiungere contenuti al nostro sito.

Ciclo di lavoro
---------------

Realizzare un progetto significa modificare un progetto.
Si può partire da un progetto vuoto, all'inizio la modifica consisterà 
principalmente nell'aggiungere elementi; 
se si parte da un progetto già avviato la modifica consisterà 
principalmente nell'adattare, cambiare e correggere. 

In ogni caso non conviene mai operare molti cambiamenti senza controllare 
il risultato:
è molto più semplice affrontare un errore-problema alla volta piuttosto che
trovarsi di fronte ad un lungo elenco di errori.

Il tipico ciclo di lavoro è costituito da due cicli annidati:

#. Modifico il progetto

   a. modifico un file ``.rst``
   b. compilo i sorgenti con: ``make html``
   c. controllo il risultato e ritorno al punto: a.
#. Fisso le modifiche apportate con: 
   
   ``git add --all``
   
   ``git commit -m "<commento>"``
#. Invio al repository le modifiche: ``git push`` e ritorno al punto 1.



