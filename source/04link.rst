Link ipertestuali
=================

Link esterni
------------

Nella sezione relativa agli strumenti, abbiamo già visto diversi esempi 
di link a altre pagine Internet.

Vediamone alcuni:

* `un tutorial su Sphinx 
  <https://techwritingmatters.com/documenting-with-sphinx-tutorial-intro-overview>`_;
* `un interessante tutorial su rest 
  <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_;
* `il repository di questo progetto 
  <https://bitbucket.org/danzam_sito10min/sito10min/>`_;
* `il sito di Matematica Dolce <https://www.matematicadolce.eu>`_.

Questa lista puntata di link è stata ottenuta con::

  * `un tutorial su Sphinx 
    <https://techwritingmatters.com/documenting-with-sphinx-tutorial-intro-overview>`_;
  * `un interessante tutorial su rest 
    <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_;
  * `il repository di questo progetto 
    <https://bitbucket.org/danzam_sito10min/sito10min/>`_;
  * `il sito di Matematica Dolce <https://www.matematicadolce.eu>`_.

Link interni al documento
-------------------------

ReStructuredText mette a disposizione i riferimenti automatici a tutte le sezioni del documento, ma, per farlo, dobbiamo modificare il file 
``conf.py`` nella riga relativa alle estensioni::

  extensions = ['sphinx.ext.autosectionlabel',]

Fatto questo posso creare un link alla sottosezione "Tabelle" come questo:

:ref:`Tabelle`

ottenuto con il comando::

  :ref:`Tabelle`

Posso anche porre dei riferimenti in punti qualsiasi del testo. 
Il seguente link porta a 

:ref:`Seconda tabella <seconda tabella>`

Il link precedente è realizzato dal comando::

  :ref:`Seconda tabella <seconda tabella>`

Il link funziona perché prima della seconda tabella ho inserito la 
riga::

  .. _seconda tabella:

Così, se prima della definizione di potenza scrivo::

  .. _definizione potenza:

posso inserire un link a quella posizione: 

:ref:`Definizione di potenza <definizione potenza>`

link realizzato dal comando::

  :ref:`Definizione di potenza <definizione potenza>`

Link a documenti in locale
--------------------------

Quando ho pensato a questo lavoro ero particolarmente interessato a 
collegare al documento creato con ``Sphinx`` altri documenti ``html`` 
realizzati con altri programmi. Qualcosa come i link seguenti.

Vedi `Espressioni Mathjax <expages/provaesp/provaesp.html>`_.

Vedi `Grafici Tikz <expages/tikz_math/tikz_math.html>`_.

Per risolvere questo problema bisogna fare un po' di lavoro.


Aggiunta al file di configurazione
........................................

In ``conf.py`` ho aggiunto la riga::

  html_extra_path = ["_pages"]


Aggiunta della dir ``_pages``
.............................

Nella directory del progetto, dove si trova il file ``index.rst`` 
ho aggiunto la directory ``_pages`` con due sottodir che contengono 
i materiali delle due pagine che voglio visualizzare.

La directory ``source`` ha ora i seguente contenuto::

  .
  ├── 00intro.rst
  ├── 01strumenti.rst
  ├── 02organizzazione.rst
  ├── 03contenuti.rst
  ├── 04link.rst
  ├── 05pubblicazione.rst
  ├── conf.py
  ├── index.rst
  ├── _pages
  │   └── expages
  │       ├── provaesp
  │       │   ├── provaesp.css
  │       │   └── provaesp.html
  │       └── tikz_math
  │           ├── tikz_math0x.svg
  │           ├── tikz_math.css
  │           └── tikz_math.html
  ├── _static
  │   └── 03img
  │       ├── libri01.jpg
  │       └── libri02.jpg
  └── _templates


Aggiunta dei link
.................

Ora posso aggiungere i link prodotti dai comandi::

  Vedi `Espressioni Mathjax <expages/provaesp/provaesp.html>`_.

  Vedi `Grafici Tikz <expages/tikz_math/tikz_math.html>`_.

Non dimentichiamoci del nostro repository
-----------------------------------------

Quando arriviamo ad un punto fermo, ma almeno una volta al giorno 
se stiamo lavorando ad un progetto, aggiorniamo il nostro repo::

  $> git status
  $> git add --all
  $> git commit -m "Modificato 03contenuti."
  $> git push
