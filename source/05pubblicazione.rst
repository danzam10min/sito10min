Pubblicazione
=============

Ora che ho realizzato il mio sito e che, in locale, lo ho 
provato e lo ho trovato funzionante, voglio pubblicarlo su internet.
Ora vediamo come pubblicare il nostro lavoro sul Clud di Bitbucket.

Bitbucket mette a disposizione di ogni utente una pagina statica 
all'indirizzo::

  <nomeutente>.bitbucket.io

Il ``<nomeutente>`` non deve contenere spazi, punti, underscore, o altri 
caratteri strani.

La documentazione che riguarda la pubblicazione si trova in:

`support.atlassian.com/bitbucket-cloud/docs/publishing-a-website-on-bitbucket-cloud/ 
<https://support.atlassian.com/bitbucket-cloud/docs/publishing-a-website-on-bitbucket-cloud/>`_.

Creazione del repository
------------------------

#. Nel nostro spazio su Bitbucket ho cliccato sul simbolo ``+`` presente 
   nella barra di sinistra in alto.
#. Poi ho cliccato su ``CREATE Repository`` creandone uno con il nome: 
   ``danzam10min.bitbucket.io``

Nel form che mi viene presentato ho operato le seguenti scelte:

Project
  ``sito``;
Repository name
  ``danzam10min.bitbucket.io``;
Access level
  ``public``;
Include a README?
  ``Yes with a template``
Include .gitignore?
  ``No``
Description:
  ``Sito di danzam10min.``

Questo repo è collegato alla pagina Internet: 
``https://danzam10min.bitbucket.io``.
Perchè questa pagina mostri qualcosa, nella radice di questo nuovo repo 
dovrà esserci il file ``index.html`` che 
sarà utilizzato come pagina iniziale.

Collegamento con la directory ``html``
--------------------------------------

Ora devo clonare in locale questo repo, per cui dalla pagina principale 
clicco sul bottone ``Clone`` in alto a destra e poi sull'icona 
``Copy``.

Mi porto nella directory del mio progetto, 
mi sposto nella sottodirectory ``build`` e:

#. rinomino la directory ``html`` chiamandola ``html_orig``
   Posso farlo da interfaccia grafica (tasto destro-Rinomina) o da 
   terminale::
   
     mv html html_orig;
#. incollo in un terminale (tasto destro del mouse e ``Incolla``) 
   il comando per clonare il repo e eseguito con invio;

Ora in ``build`` è presente la directory ``danzam10min.bitbucket.io``.

Controllo il suo contenuto:: 

  .
  ├── .git
  │   ├── branches
  │   ├── ...
  │   └── refs
  └── README.md

Contiene la dir nascosta ``.git``:
questo significa che è un progetto ``git`` e come tale andrà trattato.

Contiene anche il file ``README.md`` che modificherò.

Per il resto è piuttosto vuota, ma io ho già tutti i file che mi servono...

3. copio tutto il contenuto della dir ``html_orig`` nella cartella 
   ``danzam10min.bitbucket.io``;
4. infine rinomino ``danzam10min.bitbucket.io`` chiamandola ``html``, 
   così ogni volta che compilo il mio sorgente con ``Sphinx``, 
   questa cartella verrà aggiornata.

Aggiornamento del repository
----------------------------

Già che ci sono, nel file ``README.md`` ho scritto:

.. literalinclude:: ../build/html/README.md

Ora ho aperto un terminale nella directory ``sito10min/build/html`` 
ho dato il comando::

  $> git status

che mi mostra tutti i file aggiunti o modificati ma non ancora registrati nell'area di *staging*.

Con::

  $> git add --all

li registro, poi do un nome a questi cambiamenti e li carico nel repository remoto::

  $> git commit -m "Primo commit."
  
  $> git push

Ecco il sito!
-------------

A questo punto posso vedere il mio nuovo sito all'indirizzo:

`danzam10min.bitbucket.io <https://danzam10min.bitbucket.io>`_

Se vengono operati dei cambiamenti al sorgente e poi questi vengono 
compilati con il solito::

  $> make html
  
La dir ``build/html`` verrà aggiornata, ma il sito rifletterà queste 
modifiche **solo quando sarà aggiornato** anche il repository 
``danzam10min.bitbucket.io``

Ciclo di lavoro complessivo
---------------------------

Dato che il progetto ``sito10min`` 
contiene al suo interno, nella dir ``html``, il progetto ``danzam10min.bitbucket.io`` 
e questi due progetti hanno dei repo differenti, il ciclo completo per
modificare una pagina del sito ``danzam10min.bitbucket.io`` 
è il seguente:

#. modifico il sorgente;
#. compilo con ``Sphinx``;
#. visualizzo in un browser cliccando su ``build/html/index.html``, 
   o aggiornando la pagina del browser;
#. torno al punto 1. finché non sono soddisfatto del risultato;
#. aggiorno il repository ``sito10min`` con i soliti comandi di ``git``;
#. quando ritengo che i cambiamenti siano pubblicabili, apro un terminale 
   nella dir: ``sito10min/build/html``;
#. e con i soliti comandi di ``git`` aggiorno il repo 
   ``danzam10min.bitbucket.io``.

A questo punto il sito ``danzam10min.bitbucket.io`` riflette i cambiamenti 
effettuati.

