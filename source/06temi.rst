Temi
====

Finalmente ho la mia pagina pubblicata!

Alabaster
---------

La riapro dopo una notte di sonno ristoratore e mi concentro sulla sua struttura.

C'è una barra verticale a sinistra che contiene:

* il titolo;
* l'indice del sito;
* la casella di ricerca.

In basso c'è un piè di pagina con alcune informazioni:

* anno;
* autore;
* programma con cui è stato realizzato;
* tema;
* il sorgente della pagina.

Questo modo di presentare la pagina non l'ho deciso io, dipende dal tema 
utilizzato.
Il tema usato è ``Alabaster`` che è il tema di default di ``Sphinx``.
Ma questo tema non l'ho scelto io 
(essendo quello di *default* lo ha scelto ``Sphinx``). 
Posso vedere il tema usato guardando il piede di ogni pagina o leggendo 
il file ``conf.py``::

  html_theme = 'alabaster'

dove il tema viene invocato.

Mi piace abbastanza ma ci sono alcuni punti che vorrei cambiare.

``Alabaster`` è ben documentato, le informazioni relative a questo tema si possono trovare in:

`alabaster.readthedocs.io <https://alabaster.readthedocs.io/>`_

In particolare nella sezione: 
`Customisation 
<https://alabaster.readthedocs.io/en/latest/customization.html>`_.

Fissare la barra di Alabaster
.............................

Quando scorro una pagina, perché è più lunga di una videata, la barra 
verticale esce dalla vista, io preferirei averla sempre a portata 
di mouse.

L'opzione ``fixed_sidebar`` permette proprio di fare questo.

Navigazione con next-prev
.........................

Mi piacerebbe anche avere in testa e ai piedi di ogni pagina i bottoni che permettono di passare alla pagina seguente o precedente.

Anche per questo c'è un'opzione: ``show_relbars``.

Modifico quindi il file ``conf.py`` aggiungendo dopo la riga::

  html_theme = 'alabaster'
  
la seguente istruzione::

  html_theme_options = {
      'fixed_sidebar': 'true',
      'show_relbars': 'true',
  }

Ricompilo e osservo il risultato.

Se non vedo alcuna cambiamento forse è perché, prima di ricompilare mi sono 
dimenticato di salvarlo.
Accidenti a me, torno nell'editor, salvo le modifiche e ricompilo.

Altri temi
----------

``Sphinx`` è orientato alla documentazione e il tema di default è 
completamente privo di sfronzoli.

Sono disponibili svariati altri temi, tutti molto spartani.
Una raccolta è realizzata in:

`sphinx-themes.org <https://sphinx-themes.org/>`_

Read the docs
.............

Un altro tema da provare è quello usato da  
`readthedocs <https://readthedocs.org/>`_

Per usarlo dobbiamo installarlo sul nostro sistema::

  $> sudo pip install sphinx-rtd-theme

E poi modificare il file ``conf.py`` commentando le righe relative a 
``alabaster`` e invocando, al suo posto, il tema ``sphinx_rtd_theme``::

  ##html_theme = 'alabaster'
  ##html_theme_options = {
  ##    'fixed_sidebar': 'true',
  ##    'show_relbars': 'true',
  ##}

  html_theme = 'sphinx_rtd_theme'
  
Le righe commentate potrebbero anche essere semplicemente cancellate, 
ma se le commento è più facile tornare in dietro.

Anche questo tema è documentato e può essere configurato 
(vedi: `sphinx-rtd-theme.readthedocs.io/ 
<https://sphinx-rtd-theme.readthedocs.io/>`_)

In particolare la sezione relativa alla 
`configurazione 
<https://sphinx-rtd-theme.readthedocs.io/en/stable/configuring.html>`_

Ad esempio per avere i pulsanti ``precedente`` e ``seguente`` sia 
all'inizio sia alla fine della pagina posso aggiungere in ``conf.py``
il comando::

  html_theme_options = {'prev_next_buttons_location': 'both'
                      }

Salvo il tutto, compilo e guardo il risultato aggiornando la pagina che 
punta a: 

``file://.../sito10min/build/html/06temi.html``

