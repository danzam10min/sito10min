Strumenti
=========

Software da installare
----------------------

Prima di partire dobbiamo installare sulla nostra macchina:

* Sphinx
* Git

In questo documento faccio riferimento a un sistema `Linux`, 
perché è quello che uso. 
I comandi da dare in un terminale saranno del tipo::

  $> cd

dove ``$>`` rappresenta quanto viene scritto dal terminale stesso e si 
chiama ``prompt dei comandi`` e può essere diverso in diverse macchine; 
mentre ``cd`` è il comando effettivamente scritto dall'utente.

Un sistema `Mac` non dovrebbe essere molto diverso, mentre Windows penso 
che presenti alcune diversità più marcate (ma non lo conosco).

Possiamo controllare in un terminale se questi due pacchetti sono presenti e 
funzionanti con i comandi::

  $> sphinx-build --version
  sphinx-build 4.3.1

  $> git --version
  git version 2.33.0

Se otteniamo dei messaggi di errore invece che l'indicazione del numero di 
versione, vuol dire che non sono presenti; installiamoli usando il 
programma di gestione dei pacchetti presente nel nostro sistema.

Bitbucket
---------

Ora che abbiamo sul nostro computer il software necessario, creiamoci un 
repository remoto. 
Il sito ``bitbucket.org`` ci mette a disposizione un repository che può 
ospitare il nostro progetto e possiamo anche condividerlo con altri. 
Questi servizi sono realizzati principalmente per gestire progetti 
software. 

``bitbucket.org`` è uno dei tanti servizi di questo tipo presenti nella 
rete (il più famoso è `github.com <https://github.com/>`_)

Per poterlo utilizzare mi sono dovuto registrare e ho scelto come 
nomignolo: ``danzam10min``. 

IMPORTANTE: per il prosieguo del lavoro, non bisogna mettere caratteri 
"strani" nel nome utente e neppure nel nome del repository, 
non si devono utilizzare neanche spazi, punti o trattini bassi::
  
  No: " ", ".", "_", ...

.. L'indirizzo a cui si trova questo lavoro è:
.. 
.. `bitbucket.org/danzamsito10min/sito10min 
.. <https://bitbucket.org/danzamsito10min/sito10min/>`_
.. 
.. Essendo pubblico, chiunque può accedervi, navigare nel sorgente, 
.. scaricare un file o l'intero repository in un formato compresso o 
.. clonare il progetto e contribuirvi.

Git
---

Il repository ci fornisce gli strumenti per lavorare al nostro progetto 
*on-line*, 
Ma trovo che sia più produttivo lavorare in locale con il proprio computer 
e interfacciarsi con il repository attraverso un programma opportuno.

``Git`` è il ``Sistema di Controllo di Versione`` utilizzato per gestire 
il progetto: 
fornisce strumenti per gestire progetti in continua evoluzione e 
realizzati a più mani. 

Permette di tenere traccia di tutti i cambiamenti apportati al progetto, 
di condividerli nel repository remoto, di tornare a una precedente 
versione,...

Il tipico ciclo di lavoro con `git` è:

#. effettuare modifiche ai sorgenti;
#. controllare le modifiche con:: 

   $> git status
#. aggiungere le modifiche::

   $> git add --all
#. fissare e dare un'etichetta  a queste modifiche::

   $> git commit -m "Etichetta sensata."
#. aggiornare il repository remoto::

   $> git push

Il sito di riferimento per ``Git`` è: 

`git-scm.com <https://git-scm.com/>`_.

Sphinx
------

Il programma che trasforma i file ``.rst`` in ``.html`` è ``Sphinx``. 

Il tipico ciclo di lavoro con ``sphinx`` è:

#. Creare o modificare i sorgenti, ricordandosi di salvare i cambiamenti.
#. Compilare il progetto con::
  
   $> make html.
#. Correggere eventuali errori segnalati da ``sphinx``.
#. Aprire il file ``index.html`` in un browser 
   (o aggiornare la pagina se il file è già aperto) osservare cosa c'è 
   da cambiare o da aggiungere.

Il sito di riferimento per ``Sphinx`` è:

`www.sphinx-doc.org <https://www.sphinx-doc.org/>`_

Da lì si può accedere a una grande quantità di documentazione.
