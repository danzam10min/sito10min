Introduzione
============

In questo documento descrivo come si può realizzare un sito Internet, 
composto da pagine statiche,
usando alcuni strumenti potenti e flessibili:

*  ``ReStructuredText`` un linguaggio di Markup che ``Sphinx`` può compilare producendo
   documenti anche nel formato ``html``.
*  ``Git`` un programma che permette la gestione di codice che può essere
   depositato in uno dei tanti servizi presenti in rete io userò:
   `bitbucket.org <http://bitbucket.org/>`_.

Il ciclo di produzione consiste in alcuni passi:

#. Scrivere il contenuto del sito usando i marcatori adatti, con un qualunque
   editor di testo.
#. Compilare i file ``.rst`` producendo file ``html``, usando ``Sphinx``.
#. Controllare il risultato aprendo il file compilato in un browser.
#. Aggiornare il repository su ``bitbucket`` usando ``git``.

Tutti i programmi utilizzati sono liberi, si possono scaricare dalla rete e
installare sul proprio sistema. Per avere informazioni basta ricercare in
internet, una delle parole: 

*  ReStructuredText
*  Sphinx
*  Bitbucket
*  Git

