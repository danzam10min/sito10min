.. libro10min documentation master file, created by
   sphinx-quickstart on Wed Dec 15 22:12:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Benvenuto nella documentazione di sito10min!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   00intro
   01strumenti
   02organizzazione
   03contenuti
   04link
   05pubblicazione
   06temi

Indici
======

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
